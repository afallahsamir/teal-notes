import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Note.css';

class Note extends Component{

    constructor(props){
        super(props);
        this.state = {
            edit: false,
        };
        let data = props.noteContent;
        if(props.noteContent && props.noteContent.match(/-?\d+/g)) {
            for (let i = 0; i < props.noteContent.match(/-?\d+/g).length; i++) {
                const searchedValue = props.noteContent.match(/-?\d+/g)[i];
                const replacedValue = Math.sign(parseFloat(props.noteContent.match(/-?\d+/g)[i])) === 1 ? '<font color="green"><b>'+ parseFloat(props.noteContent.match(/-?\d+/g)[i]) +'</b></font>' : '<font color="red"><b>'+ parseFloat(props.noteContent.match(/-?\d+/g)[i]) +'</b></font>';
                data = data.replace(searchedValue, replacedValue);
            }
        }
        this.noteContent = data;
        this.noteId = props.noteId; 
        this.removeNote = this.removeNote.bind(this);
    }

    componentWillReceiveProps(nextProps){
        let data = nextProps.noteContent;
        if(nextProps.noteContent && nextProps.noteContent.match(/-?\d+/g)) {
            for (let i = 0; i < nextProps.noteContent.match(/-?\d+/g).length; i++) {
                const searchedValue = nextProps.noteContent.match(/-?\d+/g)[i];
                const replacedValue = Math.sign(parseFloat(nextProps.noteContent.match(/-?\d+/g)[i])) === 1 ? '<font color="green"><b>'+ parseFloat(nextProps.noteContent.match(/-?\d+/g)[i]) +'</b></font>' : '<font color="red"><b>'+ parseFloat(nextProps.noteContent.match(/-?\d+/g)[i]) +'</b></font>';
                data = data.replace(searchedValue, replacedValue);
            }
        }
        this.noteContent = data;
        if(this.state.edit) {
            this.setState({
                edit: false,
            });
        }
    }

    removeNote(id){
        this.props.removeNote(id);
    }

    updateNote(id, content) {
        this.setState({
            edit: id,
        });
        this.props.updateNote(id, content);
    }

    isInEdit = (noteId) => {
        let classes = "note";
        if(noteId === this.state.edit) {
            classes += " edit";
        }
        return classes;
    }

    render(){
        return(
            <div className={this.isInEdit(this.noteId)} onDoubleClick={() => this.updateNote(this.noteId, this.noteContent)}>
                <span className="close" 
                      onClick={() => this.removeNote(this.noteId)}>
                      &times;
                </span>
                <div dangerouslySetInnerHTML={{__html: this.noteContent}}></div>
            </div>
        )
    }
}

Note.propTypes = {
    noteContent: PropTypes.string
}

export default Note;
