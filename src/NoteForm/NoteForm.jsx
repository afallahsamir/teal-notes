import React, { Component } from 'react';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './NoteForm.css';


class NoteForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            editorState: EditorState.createEmpty(),
        };       

        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.writeNote = this.writeNote.bind(this);
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
    };

    handleKeyDown(event) {
        let charCode = String.fromCharCode(event.which).toLowerCase();
        if(event.ctrlKey && charCode === 's') {
            event.preventDefault();
            if(this.props.currentNote.id) {
                this.updateNote(this.props.currentNote);
            } else {
                this.writeNote();
            }
        }
        /*
        * For MAC
        */
        if(event.metaKey && charCode === 's') {
            event.preventDefault();
            if(this.props.currentNote.id) {
                this.updateNote(this.props.currentNote);
            } else {
                this.writeNote();
            }
        }
    }

    writeNote() {
        this.props.addNote(draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())));

        this.setState({
            editorState: EditorState.createEmpty(),
        })
    }

    updateNote(currentNote) {
        this.props.editNote(currentNote.id,draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())));
    }

    render(){
        const { editorState } = this.state;
        return(
            <div onKeyDown={this.handleKeyDown}>
                <Editor
                    initialEditorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    mention={{
                        separator: ' ',
                        trigger: '@',
                        suggestions: [
                          { text: 'Mehdi', value: 'mehdi', url: '#' },
                          { text: 'Yassar', value: 'yassar', url: '#' },
                        ],
                    }}
                    onEditorStateChange={this.onEditorStateChange}
                />
                <textarea
                disabled
                value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                />
            </div>
        )
    }
}

export default NoteForm;