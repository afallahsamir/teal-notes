import React, { Component } from 'react';
import Note from './Note/Note';
import NoteForm from './NoteForm/NoteForm';
import Total from './Total/Total';
import { DB_CONFIG } from './Config/config';
import firebase from 'firebase/app';
import 'firebase/database';
import './App.css';

class App extends Component {

  constructor(props){
    super(props);
    this.addNote = this.addNote.bind(this);
    this.editNote = this.editNote.bind(this);
    this.removeNote = this.removeNote.bind(this);
    this.updateNote = this.updateNote.bind(this);

    this.app = firebase.initializeApp(DB_CONFIG);
    this.database = this.app.database().ref().child('notes');

    this.state = {
      notes: [],
      currentNote: {}
    }
  }

  componentDidMount(){
    const previousNotes = this.state.notes;

    this.database.on('child_added', snap => {
      previousNotes.push({
        id: snap.key,
        noteContent: snap.val().noteContent,
      })

      this.setState({
        notes: previousNotes
      })
    })

    this.database.on('child_removed', snap => {
      for(var i=0; i < previousNotes.length; i++){
        if(previousNotes[i].id === snap.key){
          previousNotes.splice(i, 1);
        }
      }

      this.setState({
        notes: previousNotes
      })
    })

    this.database.on('child_changed', snap => {
      for(var i=0; i < previousNotes.length; i++){
        if(previousNotes[i].id === snap.key){
          previousNotes[i].noteContent = snap.val().noteContent;
        }
      }
      this.setState({
        notes: previousNotes
      })
    })
  }

  addNote(note){
    this.database.push().set({ noteContent: note.replace(/(?:\r\n|\r|\n)/g, '<br>')});
  }

  removeNote(noteId){
    this.database.child(noteId).remove();
  }

  updateNote(noteId, noteContent) {
    this.setState({
      currentNote: {id: noteId, content: noteContent}
    })
  }

  editNote(id, note) {
    this.database.child(id).update({noteContent: note});
  }

  render() {
    let total;
    if(this.state.notes.length > 0) {
      total = <div className="total"><Total content={this.state.notes}/></div>;
    }
    return (
      <div className="wrapper">
        <div className="editor">
          <NoteForm addNote={this.addNote} editNote={this.editNote} currentNote={this.state.currentNote}/>
        </div>
        <div className="notes">
          {
            this.state.notes.slice(0).reverse().map((note) => {
              return (
                <Note noteContent={note.noteContent} 
                noteId={note.id} 
                key={note.id} 
                removeNote ={this.removeNote}
                updateNote ={this.updateNote}/>
              )
            })
          }
        </div>
        {total}
      </div>
    );
  }
}

export default App;
