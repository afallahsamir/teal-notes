import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Total.css';


const Sum = (props) => {
	let numbers = '';
	for (let i = 0; i < props.content.length; i++) {
		numbers += props.content[i].noteContent && props.content[i].noteContent.match(/-?\d+/g) ? props.content[i].noteContent.match(/-?\d+/g).join(';')+";" : 0;
	}
	const _numbers = numbers.split(';').map((item) => {
		return !Number.isNaN(parseFloat(item)) ? parseFloat(item) : 0;
	});
	return _numbers.reduce((a,b) => a + b, 0);
}

class Total extends Component{

    render(){
        return(
            <div>
				<p><strong><i>TOTAL</i></strong></p>
				<p><span>{Sum(this.props)} MAD</span></p>
            </div>
        )
    }
}

Total.propTypes = {
    noteContent: PropTypes.string
}

export default Total;
